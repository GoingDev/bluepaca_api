<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('p_code')->nullable();
            $table->string('p_name_TH');
            $table->string('p_name_EN')->nullable();
            $table->text('p_detail_TH')->nullable();
            $table->text('p_detail_EN')->nullable();
            $table->text('p_attr')->nullable();
            $table->text('p_img')->nullable();
            $table->integer('p_status')->default(0);
            $table->integer('p_amount')->default(0);
            $table->integer('p_price')->default(0);
            $table->integer('p_dis_price')->nullable();
            $table->text('p_keyword')->nullable();
            $table->text('p_description')->nullable();
            $table->text('p_url')->nullable();
            $table->integer('b_id')->nullable();
            $table->integer('c_id')->nullable();
            $table->text('u_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
