<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coupon_code');
            $table->integer('coupon_type')->default(1);
            $table->boolean('coupon_usageAnotherPromotion')->default(false);
            $table->boolean('coupon_usageAnotherSale')->default(false);
            $table->text('coupon_product')->nullable();
            $table->text('coupon_category')->nullable();
            $table->text('coupon_brand')->nullable();
            $table->text('coupon_description')->nullable();
            $table->integer('coupon_discount')->default(0);
            $table->integer('coupon_minimum')->nullable();
            $table->integer('coupon_maximum')->nullable();
            $table->integer('coupon_limitCoupon')->nullable();
            $table->integer('coupon_limitUser')->nullable();
            $table->date('coupon_expirydate');
            $table->boolean('coupon_status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
