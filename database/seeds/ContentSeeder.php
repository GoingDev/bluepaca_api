<?php

use Illuminate\Database\Seeder;
use App\Content;
use Illuminate\Support\Facades\Hash;
class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $array = array(
            [
                'id'=>1,
                'content_name'=>'About',
                'content_title_TH'=>'เกี่ยวกับเรา',
                'content_title_EN'=>'About',
                'content_detail_TH'=>'<p style="text-align: center;">แรกเริ่ม</p>',
                'content_detail_EN'=>'<p style="text-align: center;">default</p>'
            ],
            [
                'id'=>2,'content_name'=>'TermService',
                'content_title_TH'=>'เงื่อนไขการใช้บริการ',
                'content_title_EN'=>'TermService',
                'content_detail_TH'=>'<p style="text-align: center;">แรกเริ่ม</p>',
                'content_detail_EN'=>'<p style="text-align: center;">default</p>'
            ],
            [
                'id'=>7,
                'content_name'=>'Policy',
                'content_title_TH'=>'นโยบายการใช้งาน',
                'content_title_EN'=>'Policy',
                'content_detail_TH'=>'<p style="text-align: center;">แรกเริ่ม</p>',
                'content_detail_EN'=>'<p style="text-align: center;">default</p>'
            ],
            [
                'id'=>3,
                'content_name'=>'Howtobuy',
                'content_title_TH'=>'สั่งซื้ออย่างไร',
                'content_title_EN'=>'Howtobuy',
                'content_detail_TH'=>'<p style="text-align: center;">แรกเริ่ม</p>',
                'content_detail_EN'=>'<p style="text-align: center;">default</p>'
            ],

            [
                'id'=>4,
                'content_name'=>'Howtopay',
                'content_title_TH'=>'วิธีการชำระเงิน',
                'content_title_EN'=>'Howtopay',
                'content_detail_TH'=>'<p style="text-align: center;">แรกเริ่ม</p>',
                'content_detail_EN'=>'<p style="text-align: center;">default</p>'
            ],
            [
                'id'=>5,
                'content_name'=>'Delivery',
                'content_title_TH'=>'การจัดส่ง',
                'content_title_EN'=>'Delivery',
                'content_detail_TH'=>'<p style="text-align: center;">แรกเริ่ม</p>',
                'content_detail_EN'=>'<p style="text-align: center;">default</p>'
            ],
            [
                'id'=>6,
                'content_name'=>'Faq',
                'content_title_TH'=>'คำถามที่พบบ่อย',
                'content_title_EN'=>'Faq',
                'content_detail_TH'=>'<p style="text-align: center;">แรกเริ่ม</p>',
                'content_detail_EN'=>'<p style="text-align: center;">default</p>'
            ]
        );
        foreach($array as $row) {
            Content::create($row);
        }
    }
}
