<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User;
        $user->name = "Admin";
        $user->email = "Administrator@goingstudio.com";
        $user->password = Hash::make("adminadmin");
        $user->status = 1;
        $user->save();
    }
}
