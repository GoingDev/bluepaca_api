<?php

use Illuminate\Database\Seeder;
use App\Bannerhome;
class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $array = array(
            ['bh_type'=>1],
            ['bh_type'=>4],
            ['bh_type'=>2],
            ['bh_type'=>2],
            ['bh_type'=>3],
            ['bh_type'=>3],
            ['bh_type'=>3],
            ['bh_type'=>3],
            ['bh_type'=>3],
            ['bh_type'=>3]
        );
        foreach($array as $row) {
            Bannerhome::create($row);
        }
    }
}
