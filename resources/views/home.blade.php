@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">สำเร็จ !</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    แก้ไขรหัสผ่านเรียบร้อยแล้ว !
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
