<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductComment extends Model
{
    //
    protected $table = 'product_comments';
    protected $casts = [
        'pc_like' => 'array',
    ];

    public function Users(){
        return $this->hasOne('App\User','id','owner_id');
    }
}
