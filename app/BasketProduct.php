<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasketProduct extends Model
{
    //
    public function Product(){
        return $this->hasOne('App\Product','id','p_id');
    }
}
