<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bannerhome;
class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Bannerhome::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $file = $request->file;
        $img = null;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('Picture/BannerPicture'), $fileName);
            $img = $fileName;
        }
        $form = json_decode($request->form);
        $banner = new Bannerhome;
        $banner->bh_img = $img;
        $banner->bh_link = $form->bh_link;
        $banner->bh_type = $form->bh_type;
        $banner->save();
        return response()->json(['success'=>'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $file = $request->file;
        $img = null;
        $form = json_decode($request->form);
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('Picture/BannerPicture'), $fileName);
            $img = $fileName;
            $deskt_file = base_path().'/public/Picture/BannerPicture/'.$form->bh_img;
            if (file_exists($deskt_file) && $form->bh_img != null)
			{
				unlink($deskt_file);
			}
        }
        else{
            $img = $form->bh_img;
        }
        $banner = Bannerhome::find($id);
        $banner->bh_img = $img;
        $banner->bh_link = $form->bh_link;
        $banner->bh_type = $form->bh_type;
        $banner->save();
        return response()->json(['success'=>'done']);
    }
    public function bannerUpdateIndex(Request $request,$type)
    {
        Bannerhome::where('bh_type',$type)->delete();
        $data = json_decode($request->params);
        foreach($data as $value){
            $banner = new Bannerhome;
            $banner->bh_img = $value->bh_img;
            $banner->bh_link = $value->bh_link;
            $banner->bh_type = $type;
            $banner->save();
        }
        return response()->json(['success'=>'done']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $banner = Bannerhome::find($id);
        $deskt_file = base_path().'/public/Picture/BannerPicture/'.$banner->bh_img;
        if (file_exists($deskt_file) && $banner->bh_img != null){
			unlink($deskt_file);
        }
        Bannerhome::destroy($id);
        return response()->json(['success'=>'done']);
    }
    public function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
