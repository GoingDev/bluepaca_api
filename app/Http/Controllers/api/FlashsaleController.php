<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Flashsale;
use App\FlashsaleDetail;
class FlashsaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return Flashsale::with(['FlashsaleDetail.Product.Brand'])->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $name = $request->name;
        $date = $request->date;
        $time = str_split($request->time);
        $product = json_decode($request->product);
        $fs = new Flashsale;
        $fs->fs_name = $name;
        $fs->fs_dateend = $date." ".$time[0].$time[1].":".$time[2].$time[3].":00";
        $fs->fs_status = 0;
        $fs->save();
        $id = $fs->id;
        foreach($product as $val){
            $fsd = new FlashsaleDetail;
            $fsd->fsd_price = $val->discount;
            $fsd->fs_id = $id;
            $fsd->p_id = $val->id;
            $fsd->save();
        }
        return response()->json(['success'=>'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        return Flashsale::with(['FlashsaleDetail.Product.Brand'])->where('fs_status',1)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $fsClose = Flashsale::where('fs_status',1)->update(['fs_status'=>0]);
        $fs = Flashsale::find($id);
        $fs->fs_status = $request->params['status'];
        $fs->save();
        return response()->json(['success'=>'done']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Flashsale::destroy($id);
        FlashsaleDetail::where('fs_id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
}
