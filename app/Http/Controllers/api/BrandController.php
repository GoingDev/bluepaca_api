<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Product;
class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Brand::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $file = $request->file;
        $img = null;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('Picture/BrandPicture'), $fileName);
            $img = $fileName;
        }
        $form = json_decode($request->form);
        $brand = new Brand;
        $brand->brand_name_TH = $form->brand_name_TH;
        $brand->brand_name_EN = $form->brand_name_EN;
        $brand->brand_detail_TH = $form->brand_detail_TH;
        $brand->brand_detail_EN = $form->brand_detail_EN;
        $brand->brand_img = $img;
        $brand->save();
        return response()->json(['success'=>'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $file = $request->file;
        $form = json_decode($request->form);
        $img = null;
        $level = 1;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('Picture/BrandPicture'), $fileName);
            $img = $fileName;
            $deskt_file = base_path().'/public/Picture/BrandPicture/'.$form->brand_img;
            if (file_exists($deskt_file) && $form->brand_img != null)
			{
				unlink($deskt_file);
			}
        }
        if($img == null){
            $Update = Brand::find($id);
            $Update->brand_name_TH = $form->brand_name_TH;
            $Update->brand_name_EN = $form->brand_name_EN;
            $Update->brand_detail_TH = $form->brand_detail_TH;
            $Update->brand_detail_EN = $form->brand_detail_EN;
            $Update->save();
        }else{
            $Update = Brand::find($id);
            $Update->brand_name_TH = $form->brand_name_TH;
            $Update->brand_name_EN = $form->brand_name_EN;
            $Update->brand_detail_TH = $form->brand_detail_TH;
            $Update->brand_detail_EN = $form->brand_detail_EN;
            $Update->brand_img = $img;
            $Update->save();
        }

        return response()->json(['success'=>'done']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $brand = Brand::find($id);
        $deskt_file = base_path().'/public/Picture/BrandPicture/'.$brand->brand_img;
        if (file_exists($deskt_file) && $brand->brand_img != null){
			unlink($deskt_file);
        }
        Product::where('b_id',$id)->update(['b_id'=>null]);
        Brand::destroy($id);
        return response()->json(['success'=>'done']);
    }
    public function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
