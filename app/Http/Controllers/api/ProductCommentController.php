<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductComment;
class ProductCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $newcomment = new ProductComment;
        $newcomment->pc_comment = $request->comment;
        $newcomment->p_id = $request->p_id;
        $newcomment->owner_id = $request->u_id;
        $newcomment->pc_point = $request->point;
        $newcomment->save();

        return response()->json(['sucess'=>'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $comment = ProductComment::find($id);
        $like = $comment->pc_like;
        try{ // not null

            $key = array_search($request->u_id, $like);
            if($key !== false) {
                unset($like[$key]);
            }
            else {
                array_push($like,$request->u_id);
            }

        } catch(\Exception $e) { // null

            $like = array();
            array_push($like,$request->u_id);

        }
        $comment->pc_like = $like;
        $comment->save();
        return response()->json(['success' => 'done']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = ProductComment::destroy($id);
        return response()->json(['success'=>'done']);
    }
}
