<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Address;
class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $newAddress = new Address;
        $newAddress->district = $request->district ;
        $newAddress->amphoe = $request->amphoe ;
        $newAddress->province = $request->province ;
        $newAddress->zipcode = $request->zipcode ;
        $newAddress->address = $request->address ;
        $newAddress->receiver = $request->receiver ;
        $newAddress->tel = $request->tel ;
        $newAddress->u_id = $request->u_id;
        $newAddress->save();
        return response()->json(['success' => 'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return Address::where('u_id',$id)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $editAddress = Address::find($id);
        $editAddress->district = $request->district ;
        $editAddress->amphoe = $request->amphoe ;
        $editAddress->province = $request->province ;
        $editAddress->zipcode = $request->zipcode ;
        $editAddress->address = $request->address ;
        $editAddress->receiver = $request->receiver ;
        $editAddress->tel = $request->tel ;
        $editAddress->u_id = $request->u_id;
        $editAddress->save();
        return response()->json(['success' => 'done']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Address::destroy($id);
        return response()->json(['success' => 'done']);
    }
}
