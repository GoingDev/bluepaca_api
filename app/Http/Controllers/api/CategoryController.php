<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Category::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $file = $request->file;
        $img = null;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('Picture/CategoryPicture'), $fileName);
            $img = $fileName;
        }
        $form = json_decode($request->form);
        $level = 1;
        $category = new Category;
        $category->c_name_TH = $form->c_name_TH;
        $category->c_name_EN = $form->c_name_EN;
        $category->c_subid = $form->c_subid;
        $category->c_img = $img;
        $category->save();
        return response()->json(['success'=>'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return Category::with(['Sub'])->where('id',$id)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $file = $request->file;
        $form = json_decode($request->form);
        $img = null;
        $level = 1;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('Picture/CategoryPicture'), $fileName);
            $img = $fileName;
            $deskt_file = base_path().'/public/Picture/CategoryPicture/'.$form->c_img;
            if (file_exists($deskt_file) && $form->c_img != null)
			{
				unlink($deskt_file);
			}
        }
        if($img == null){
            $Update = Category::find($id);
            $Update->c_name_TH = $form->c_name_TH;
            $Update->c_name_EN = $form->c_name_EN;
            $Update->c_subid = $form->c_subid;
            $Update->save();
        }else{
            $Update = Category::find($id);
            $Update->c_name_TH = $form->c_name_TH;
            $Update->c_name_EN = $form->c_name_EN;
            $Update->c_subid = $form->c_subid;
            $Update->c_img = $img;
            $Update->save();
        }

        return response()->json(['success'=>'done']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cate = Category::find($id);
        $deskt_file = base_path().'/public/Picture/CategoryPicture/'.$cate->c_img;
        if (file_exists($deskt_file) && $cate->c_img!=null){
			unlink($deskt_file);
        }
        Product::where('c_id',$id)->update(['c_id'=>0]);
        Category::where('c_subid',$id)->update(['c_subid'=>$cate->c_subid]);
        Category::destroy($id);
        return response()->json(['success'=>'done']);
    }
    public function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
