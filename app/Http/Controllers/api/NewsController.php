<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return News::orderBy('created_at','DESC')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $file = $request->file;
        $img = null;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('Picture/NewsPicture'), $fileName);
            $img = $fileName;
        }
        $form = json_decode($request->form);
        $news = new News;
        $news->news_title_TH = $form->news_title_TH;
        $news->news_title_EN = $form->news_title_EN;
        $news->news_detail_TH = $form->news_detail_TH;
        $news->news_detail_EN = $form->news_detail_EN;
        $news->news_status = $form->news_status;
        $news->news_img = $img;
        $news->save();
        return response()->json(['success'=>'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $file = $request->file;
        $form = json_decode($request->form);
        $img = null;
        $level = 1;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('Picture/NewsPicture'), $fileName);
            $img = $fileName;
            $deskt_file = base_path().'/public/Picture/NewsPicture/'.$form->news_img;
            if (file_exists($deskt_file) && $form->news_img != null)
			{
				unlink($deskt_file);
			}
        }
        if($img == null){
            $Update = News::find($id);
            $Update->news_title_TH = $form->news_title_TH;
            $Update->news_title_EN = $form->news_title_EN;
            $Update->news_detail_TH = $form->news_detail_TH;
            $Update->news_detail_EN = $form->news_detail_EN;
            $Update->news_status = $form->news_status;
            $Update->save();
        }else{
            $Update = News::find($id);
            $Update->news_title_TH = $form->news_title_TH;
            $Update->news_title_EN = $form->news_title_EN;
            $Update->news_detail_TH = $form->news_detail_TH;
            $Update->news_detail_EN = $form->news_detail_EN;
            $Update->news_status = $form->news_status;
            $Update->news_img = $img;
            $Update->save();
        }

        return response()->json(['success'=>'done']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $news = News::find($id);
        $deskt_file = base_path().'/public/Picture/NewsPicture/'.$news->news_img;
        if (file_exists($deskt_file) && $news->news_img != null){
			unlink($deskt_file);
        }
        News::destroy($id);
        return response()->json(['success'=>'done']);
    }
    public function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
