<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Coupon;
use Carbon\Carbon;
class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $UpdateCoupon = Coupon::whereDate('coupon_expirydate','<', Carbon::today())->where('coupon_status',1)->update(['coupon_status'=>0]);
        return Coupon::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $form = json_decode($request->form);
        $coupon = new Coupon;
        $coupon->coupon_code = $form->code;
        $coupon->coupon_type = $form->type;
        $coupon->coupon_usageAnotherPromotion = $form->usageAnotherPromotion;
        $coupon->coupon_usageAnotherSale = $form->usageAnotherSale;
        $coupon->coupon_product = $form->product;
        $coupon->coupon_category = $form->category;
        $coupon->coupon_brand = $form->brand;
        $coupon->coupon_description = $form->description;
        $coupon->coupon_discount = $form->discount;
        $coupon->coupon_minimum = $form->minimum;
        $coupon->coupon_maximum = $form->maximum;
        $coupon->coupon_limitCoupon = $form->limitCoupon;
        $coupon->coupon_limitUser = $form->limitUser;
        $coupon->coupon_expirydate = $form->expirydate;
        $coupon->save();
        return response()->json(['success'=>'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $coupon = Coupon::where('coupon_code',$request->coupon)->where('coupon_status',1)->first();
        if($coupon == '') {
            abort(404);
        } else if($coupon != '' && $coupon->coupon_limitCoupon == null && $coupon->coupon_limitUser == null){
            return $coupon;
        } else{
            if($coupon->coupon_limitCoupon != null){
                $limit = $coupon->coupon_limitCoupon;
                $find = 15;
                if($limit < $find){
                    return response()->json(['limit'=>'คูปองนี้มีผู้ใช้งานครบแล้ว']);
                }
            }
            if($coupon->coupon_limitUser != null){
                $id = $coupon->id;
                $limit = $coupon->coupon_limitUser;
                $find = 15;
                if($limit < $find){
                    return response()->json(['limit'=>'คุณได้ใช้คูปองนี้ครบแล้ว']);
                }
            }
            return $coupon;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $coupon = Coupon::find($id);
        $coupon->coupon_status = $request->params['status'];
        $coupon->save();
        return response()->json(['success'=>'done']);
    }
    public function edit(Request $request, $id)
    {
        $form = json_decode($request->form);
        $coupon = Coupon::find($id);
        $coupon->coupon_code = $form->coupon_code;
        $coupon->coupon_type = $form->coupon_type;
        $coupon->coupon_usageAnotherPromotion = $form->coupon_usageAnotherPromotion;
        $coupon->coupon_usageAnotherSale = $form->coupon_usageAnotherSale;
        $coupon->coupon_product = $form->coupon_product;
        $coupon->coupon_category = $form->coupon_category;
        $coupon->coupon_brand = $form->coupon_brand;
        $coupon->coupon_description = $form->coupon_description;
        $coupon->coupon_discount = $form->coupon_discount;
        $coupon->coupon_minimum = $form->coupon_minimum;
        $coupon->coupon_maximum = $form->coupon_maximum;
        $coupon->coupon_limitCoupon = $form->coupon_limitCoupon;
        $coupon->coupon_limitUser = $form->coupon_limitUser;
        $coupon->coupon_expirydate = $form->coupon_expirydate;
        $coupon->save();
        return response()->json(['success'=>'done']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Coupon::destroy($id);
        return response()->json(['success'=>'done']);
    }
}
