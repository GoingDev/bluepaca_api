<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\ImgProduct;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Product::with(['Category','Brand','ImgProduct','Flashsale.FlashSale'])->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $file = $request->file;
        $img = null;
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('Picture/MainProductPicture'), $fileName);
            $img = $fileName;
        }
        $form = json_decode($request->form);
        $product = new Product;
        $product->p_code = $form->code;
        $product->p_name_TH = $form->name_TH;
        $product->p_name_EN = $form->name_EN;
        $product->p_detail_TH = $form->detail_TH;
        $product->p_detail_EN = $form->detail_EN;
        $product->p_attr = $form->attr;
        $product->p_img = $img;
        $product->p_status = $form->status;
        $product->p_amount = $form->amount;
        $product->p_price = $form->price;
        $product->p_dis_price = $form->dis_price;
        $product->p_keyword = $form->keyword;
        $product->p_description = $form->description;
        $product->p_url = "https://bluepaca/products/".$form->code;
        $product->b_id = $form->b_id;
        $product->c_id = $form->c_id;
        $product->save();
        $id = $product->id;
        try{
            foreach($request->gallary as $obj){
                $ex1 = explode(".",$obj->getClientOriginalName());
                $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
                $obj->move(public_path('Picture/SubProductPicture'), $fileName);
                $subimg = $fileName;
                $ip = new ImgProduct;
                $ip->ip_img = $subimg;
                $ip->p_id = $id;
                $ip->save();
            }
        }
        catch(\Exception $e){}
        return response()->json(['success'=>'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Product::find($id)->ImgProduct;
    }

    public function showProduct($id)
    {
        return Product::where('id', $id)->with(['Category','Brand','ImgProduct','Comments.Users','Flashsale.FlashSale'])->first();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $imgdelete = ImgProduct::find($request->delete);
        try{
            foreach($imgdelete as $value){
                $deskt_file = base_path().'/public/Picture/SubProductPicture/'.$value->ip_img;
                if (file_exists($deskt_file)){
                    unlink($deskt_file);
                }
                ImgProduct::destroy($value->id);
            }
        }
        catch(\Exception $e){}
        $file = $request->file;
        $img = null;
        $form = json_decode($request->form);
        if($request->hasFile('file')){
            $ex1 = explode(".",$file->getClientOriginalName());
            $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
            $file->move(public_path('Picture/MainProductPicture'), $fileName);
            $img = $fileName;
            $deskt_file = base_path().'/public/Picture/MainProductPicture/'.$form->p_img;
            if (file_exists($deskt_file) && $form->p_img != null)
			{
				unlink($deskt_file);
			}
        }
        else{
            $img = $form->p_img;
        }
        $dis = $form->p_dis_price;
        if($form->p_status != 4){
            $dis = null;
        }
        $product = Product::find($id);
        $product->p_code = $form->p_code;
        $product->p_name_TH = $form->p_name_TH;
        $product->p_name_EN = $form->p_name_EN;
        $product->p_detail_TH = $form->p_detail_TH;
        $product->p_detail_EN = $form->p_detail_EN;
        $product->p_attr = $form->p_attr;
        $product->p_img = $img;
        $product->p_status = $form->p_status;
        $product->p_amount = $form->p_amount;
        $product->p_price = $form->p_price;
        $product->p_dis_price = $dis;
        $product->p_keyword = $form->p_keyword;
        $product->p_description = $form->p_description;
        $product->p_url = "https://bluepaca/products/".$form->p_code;
        $product->b_id = $form->b_id;
        $product->c_id = $form->c_id;
        $product->save();
        try{
            foreach($request->gallary as $obj){
                $ex1 = explode(".",$obj->getClientOriginalName());
                $fileName = time().$this->clean(base64_encode($ex1[0])).".jpg";
                $obj->move(public_path('Picture/SubProductPicture'), $fileName);
                $subimg = $fileName;
                $ip = new ImgProduct;
                $ip->ip_img = $subimg;
                $ip->p_id = $id;
                $ip->save();
            }
        }
        catch(\Exception $e){}
        return response()->json(['success'=>'done']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $product = Product::find($id);
        $imgProduct = Product::find($id)->ImgProduct;
        $deskt_file = base_path().'/public/Picture/MainProductPicture/'.$product->p_img;
        if (file_exists($deskt_file) && $product->p_img != null){
			unlink($deskt_file);
        }
        try{
            foreach($imgProduct as $img){
                ImgProduct::destroy($img->id);
                $deskt_file = base_path().'/public/Picture/SubProductPicture/'.$img->ip_img;
                if (file_exists($deskt_file)){
                    unlink($deskt_file);
                }
            }
        } catch(\Exception $e){}
        Product::destroy($id);
        return response()->json(['success'=>'done']);
    }
    public function duplicate(Request $request, $id)
    {
        $code = $request->params['code'];
        $product_copy = Product::find($id);
        $imgProduct = Product::find($id)->ImgProduct;
        $main_img = null;
        if ($product_copy->p_img != null){
            $fileCopy = $this->filecopy($product_copy->p_img,'MainProductPicture');
            if($fileCopy != false){
                $main_img = $fileCopy;
            }
        }
        $product = new Product;
        $product->p_code = $code;
        $product->p_name_TH = $product_copy->p_name_TH;
        $product->p_name_EN = $product_copy->p_name_EN;
        $product->p_detail_TH = $product_copy->p_detail_TH;
        $product->p_detail_EN = $product_copy->p_detail_EN;
        $product->p_attr = $product_copy->p_attr;
        $product->p_img = $main_img;
        $product->p_status = $product_copy->p_status;
        $product->p_amount = $product_copy->p_amount;
        $product->p_price = $product_copy->p_price;
        $product->p_dis_price = $product_copy->p_dis_price;
        $product->p_keyword = $product_copy->p_keyword;
        $product->p_description = $product_copy->p_description;
        $product->p_url = "https://bluepaca/products/".$code;
        $product->b_id = $product_copy->b_id;
        $product->c_id = $product_copy->c_id;
        $product->save();
        $id = $product->id;
        try{
            foreach($imgProduct as $img){
                $fileCopy = $this->filecopy($img->ip_img,'SubProductPicture');
                if($fileCopy != false){
                    $ip = new ImgProduct;
                    $ip->ip_img = $fileCopy;
                    $ip->p_id = $id;
                    $ip->save();
                }
            }
        } catch(\Exception $e){}
        return response()->json(['success'=>'done']);
    }
    public function filecopy($fileName, $path)
    {
        $ex1 = explode(".",$fileName);
        $fileCopy = time().$this->clean(base64_encode($ex1[0])).".jpg";
        $src = base_path().'/public/Picture/'.$path.'/'.$fileName;
        $dst = base_path().'/public/Picture/'.$path.'/'.$fileCopy;
        if (file_exists($src)){
            if (!copy($src, $dst)) {
                return false;
            }
            else{
                return $fileCopy;
            }
        }
      }
    public function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}
