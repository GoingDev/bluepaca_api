<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MailBox;
use Mail;

class MailBoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return MailBox::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $mailbox = new MailBox;
        $mailbox->mb_name = $request->name;
        $mailbox->mb_subject = $request->subject;
        $mailbox->mb_message = $request->message;
        $mailbox->mb_email = $request->email;
        $mailbox->save();
        return response()->json(['success'=>'done']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $from = MailBox::find($id);
        $data = array('name'=>$from->mb_name, 'reply' => $request->message);
        Mail::send('mailbox', $data, function($message) use ($from) {
            $message->to($from->mb_email, $from->mb_name)->subject
               ($from->mb_subject);
            $message->from('bluepaca@gmail.com','Bluepaca Administrator');
         });
         return response()->json(['sucess'=>'done']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = MailBox::destroy($id);
        return response()->json(['sucess'=>'done']);
    }
}
