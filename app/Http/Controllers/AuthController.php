<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterFormRequest;
use Auth;
use App\FollowProduct;
use App\BasketProduct;
class AuthController extends Controller
{
    //
    public function register(RegisterFormRequest $request){
        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->status = 2;
        $user->save();
        $credentials = $request->only('email', 'password');
        if(!$token = JWTAuth::attempt($credentials)){
            return response([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'msg' => 'Invalid Credentials.'
            ], 400);
        }
        $sendUser = User::where('id',$user->id)->with(['Follow.Product.Brand','Basket.Product.Brand','Basket.Product.Flashsale.FlashSale'])->first();
        return  response([
                    'status' => 'success',
                    'token' => $token,
                    'data' => $sendUser
                ])
                ->header('Authorization',$token);
    }
    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        if ( ! $token = JWTAuth::attempt($credentials)) {
                return response([
                    'status' => 'error',
                    'error' => 'invalid.credentials',
                    'msg' => 'Invalid Credentials.'
                ], 400);
        }
        $sendUser = User::where('id',$request->user()->id)->with(['Follow.Product.Brand','Basket.Product.Brand','Basket.Product.Flashsale.FlashSale'])->first();
        return response([
                'status' => 'success',
                'token' => $token,
                'data' => $sendUser
            ])
            ->header('Authorization',$token);
    }
    public function user(Request $request){
        $user = User::where('id',Auth::user()->id)->with(['Follow.Product.Brand','Basket.Product.Brand','Basket.Product.Flashsale.FlashSale'])->first();
        return response([
                'status' => 'success',
                'data' => $user
            ]);
    }
    public function refresh(){
        return response([
                'status' => 'success'
            ]);
    }
    public function logout(){
        JWTAuth::invalidate();
        return response([
                'status' => 'success',
                'msg' => 'Logged out Successfully.'
            ], 200);
    }
    public function forgetpassword(Request $request){
        $email = $request->email;
    }
    public function follow(Request $request){
        $u_id = $request->u_id;
        $p_id = $request->p_id;
        $check_follow = FollowProduct::where('u_id',$u_id)->where('p_id',$p_id)->first();
        if($check_follow){
            FollowProduct::destroy($check_follow->id);
        }else{
            $newFP = new FollowProduct;
            $newFP->u_id = $u_id;
            $newFP->p_id = $p_id;
            $newFP->save();
        }
        return FollowProduct::with(['Product.brand'])->get();
    }
    public function basket(Request $request){
        $u_id = $request->u_id;
        $p_id = $request->p_id;
        $qty = $request->qty;
        $check_basket = BasketProduct::where('u_id',$u_id)->where('p_id',$p_id)->first();
        if($check_basket){
            $update = BasketProduct::where('id',$check_basket->id)->increment('qty');
        }else{
            $newFP = new BasketProduct;
            $newFP->u_id = $u_id;
            $newFP->p_id = $p_id;
            $newFP->save();
        }
        return BasketProduct::with(['Product.brand','Product.Flashsale.FlashSale'])->get();
    }
    public function updateBasketQty(Request $request){
        $id = $request->id;
        $status = $request->status;
        if($status == 1){
            $bk = BasketProduct::where('id',$id)->increment('qty');
        }else if($status == 2){
            $bk = BasketProduct::where('id',$id)->decrement('qty');
        }
        return response()->json(['success'=>'done']);
    }
    public function remove_basket($id){
        BasketProduct::destroy($id);
        return BasketProduct::with(['Product.brand','Product.Flashsale.FlashSale'])->get();
    }
    public function clear_basket($id) {
        BasketProduct::where('u_id',$id)->delete();
        return response()->json(['success'=>'done']);
    }
}
