<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flashsale extends Model
{
    //
    protected $table = 'flashsales';
    public $timestamps = false;

    public function FlashsaleDetail(){
        return $this->hasMany('App\FlashsaleDetail','fs_id');
    }
}
