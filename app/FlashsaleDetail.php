<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlashsaleDetail extends Model
{
    //
    protected $table = 'flashsale_details';
    public $timestamps = false;

    public function Product()
    {
        return $this->hasOne('App\Product','id','p_id');
    }

    public function FlashSale()
    {
        return $this->hasOne('App\Flashsale','id','fs_id')->where('fs_status',1);
    }
}
