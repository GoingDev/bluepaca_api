<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImgProduct extends Model
{
    //
    public $timestamps = false;
    protected $table = 'img_products';

    public function product()
    {
        return $this->belongsTo('App\Product', 'p_id');
    }
}
