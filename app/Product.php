<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Product extends Model
{
    //
    protected $table = 'products';

    public function Category() {
        return $this->hasOne('App\Category','id','c_id');
    }
    public function Brand() {
        return $this->hasOne('App\Brand','id','b_id');
    }
    public function ImgProduct() {
        return $this->hasMany('App\ImgProduct','p_id');
    }

    public function Comments() {
        return $this->hasMany('App\ProductComment','p_id');
    }

    public function FlashSale() {
        return $this->hasMany('App\FlashsaleDetail','p_id');
    }
    protected $casts = [
        'u_id' => 'array',
        'p_keyword' => 'array'
    ];

}
