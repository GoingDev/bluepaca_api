<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bannerhome extends Model
{
    //
    protected $table = 'bannerhomes';
    public $timestamps = false;
}
