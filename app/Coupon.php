<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    //
    protected $table = 'coupons';
    protected $casts = [
        'coupon_product' => 'array',
        'coupon_category' => 'array',
        'coupon_brand' => 'array',
    ];
}
