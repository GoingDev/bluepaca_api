<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'api', 'as' => 'api.'], function () {
    Route::post('category/{id}', 'CategoryController@update');
    Route::apiResource('category', 'CategoryController');
    Route::post('brand/{id}', 'BrandController@update');
    Route::apiResource('brand', 'BrandController');
    Route::get('showProduct/{id}', 'ProductController@showProduct');
    Route::post('product/{id}', 'ProductController@update');
    Route::put('product/{id}', 'ProductController@duplicate');
    Route::apiResource('product', 'ProductController');
    Route::get('flashsalenow','FlashsaleController@show');
    Route::apiResource('flashsale', 'FlashsaleController');
    Route::post('coupon/{id}', 'CouponController@edit');
    Route::get('couponByText','CouponController@show');
    Route::apiResource('coupon', 'CouponController');
    Route::post('news/{id}', 'NewsController@update');
    Route::apiResource('news', 'NewsController');
    Route::apiResource('content', 'ContentController');
    Route::post('banner/{id}', 'BannerController@update');
    Route::put('bannerUpdateIndex/{type}','BannerController@bannerUpdateIndex');
    Route::apiResource('banner', 'BannerController');
    Route::apiResource('productComments', 'ProductCommentController');
    Route::apiResource('mailboxs', 'MailBoxController');
    Route::apiResource('addresses', 'AddressController');
});

Route::post('auth/register', 'AuthController@register');
Route::post('auth/login', 'AuthController@login');
Route::put('follow', 'AuthController@follow');
Route::put('basket', 'AuthController@basket');
Route::delete('remove_basket/{id}','AuthController@remove_basket');
Route::delete('clear_basket/{id}','AuthController@clear_basket');
Route::put('updateBasketQty', 'AuthController@updateBasketQty');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
 Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => 'jwt.refresh'], function(){
  Route::get('auth/refresh', 'AuthController@refresh');
});
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('auth/user', 'AuthController@user');
    Route::post('auth/logout', 'AuthController@logout');
 });

